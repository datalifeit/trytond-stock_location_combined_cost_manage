# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .cost_manage import CostAllocationPattern, CostAllocationPatternCombinedLocation


def register():
    Pool.register(
            CostAllocationPattern,
            CostAllocationPatternCombinedLocation,
            module='stock_location_combined_cost_manage', type_='model')
